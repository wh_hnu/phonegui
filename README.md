# phonegui

介绍
一款基于LVGL开发的手机界面模仿程序
在ESP32-S3平台下测试通过。
软件架构
软件架构说明
基于LVGL8.3.5开发
安装教程

1.  将ui文件夹复制到main文件夹下。
2.  在main.c里添加#include "ui.h"
3.  调用函数ui_init();
3.  编辑main文件夹下的CMakeLists.txt
4.  添加file(GLOB_RECURSE SRC_UI ${CMAKE_SOURCE_DIR} "ui/*.c")
#### 使用说明


1.  屏幕分辨率是根据320*480，3.5寸制作而成的。其他分辨率下会显示不正常。
2.  祝你好运。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一d档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
